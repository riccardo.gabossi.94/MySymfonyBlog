<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PostController extends Controller
{
    /**
     * @Route("/post", name="view_posts_route")
     */
    public function showAllPostsAction(Request $request)
    {
        // replace this example code with whatever you need
        $posts= $this->getDoctrine()->getRepository('AppBundle:Post')->findAll();

        return $this->render('pages/index.html.twig', ['posts'=>$posts]);
    }

    /**
     * @Route("/create", name="create_post_route")
     */
    public function createPostAction(Request $request)
    {
        $post = new Post;
        $form = $this -> createFormBuilder($post)
            ->add('title', TextType::class, array('attr'=> array('class'=>'form-control')))
            ->add('desccription', TextareaType::class, array('attr'=> array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('attr'=> array('class'=>'btn btn-primary', 'style' => 'margin-top:10px')))
        ->getForm();

        $form-> handleRequest($request);

        if ($form-> isSubmitted() && $form-> isValid()){
            $title = $form['title']-> getData();
            $desccription = $form['desccription']-> getData();

            $post-> setTitle($title);
            $post-> setDesccription($desccription);

            $em = $this->getDoctrine()->getManager();
            $em -> persist($post);
            $em -> flush();

            $this -> addFlash('message', 'Post creato con successo');
            return $this->redirecttoRoute('view_posts_route');
        }


        return $this->render('pages/createPost.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
